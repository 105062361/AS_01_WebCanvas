window.onload = function()
{
    init();
}

function init()
{
    var canvas = document.querySelector("#canvas");
    var context = canvas.getContext("2d");

    canvas.height = window.innerHeight * 0.6;
    canvas.width = window.innerWidth * 0.7;

    var curTool = "eraser";
    // var pickColor = document.querySelector("#selectColor");
    // var curColor = pickColor.value;

    // pickColor.addEventListener("click", function(){
    //     alert("pickColor");
    //     // curColor = pickColor.value;
    //     // var curColor = document.getElementById("selectColor").value;
    //     // curColor = "'" + curColor + "'"
    //     // document.getElementById("demo").innerHTML = curColor;
    // });
    var Undo = document.querySelector("#redo");
    Undo.addEventListener("click", function(){
        curTool = "redo"; 
        Eraser.classList.remove("selected");
        Brush.classList.remove("selected");
        Reset.classList.remove("selected");
        Save.classList.remove("selected");
        Rect.classList.remove("selected");
        Tri.classList.remove("selected");
        Cir.classList.remove("selected");
        remove_canvas(canvas);
        remove_rect(canvas);
        remove_text(canvas);
        remove_tri(canvas);
        remove_cir(canvas);
        history.redo(canvas, context);
    });
    
    var Undo = document.querySelector("#undo");
    Undo.addEventListener("click", function(){
        curTool = "undo"; 
        Eraser.classList.remove("selected");
        Brush.classList.remove("selected");
        Reset.classList.remove("selected");
        Save.classList.remove("selected");
        Rect.classList.remove("selected");
        Tri.classList.remove("selected");
        Cir.classList.remove("selected");
        remove_canvas(canvas);
        remove_rect(canvas);
        remove_text(canvas);
        remove_tri(canvas);
        remove_cir(canvas);
        history.undo(canvas, context);
    });
    
    var Upload = document.querySelector("#uploadBtn");
    Upload.addEventListener("click", function(){
        curTool = "uploadBtn"; 
        Eraser.classList.remove("selected");
        Brush.classList.remove("selected");
        Reset.classList.remove("selected");
        Save.classList.remove("selected");
        Rect.classList.remove("selected");
        Tri.classList.remove("selected");
        Cir.classList.remove("selected");
        remove_canvas(canvas);
        remove_rect(canvas);
        remove_text(canvas);
        remove_tri(canvas);
        remove_cir(canvas);
        add_img();
    });
    
    var Text = document.querySelector("#text");
    Text.addEventListener("click", function(){
        curTool = "text"; 
        //alert("text");
        Eraser.classList.remove("selected");
        Brush.classList.remove("selected");
        Reset.classList.remove("selected");
        Save.classList.remove("selected");
        Rect.classList.remove("selected");
        Tri.classList.remove("selected");
        Cir.classList.remove("selected");
        this.classList.add("selected");
        remove_canvas(canvas);
        remove_rect(canvas);
        add_text(canvas);
        remove_tri(canvas);
        remove_cir(canvas);
    });

    var Cir = document.querySelector("#circle");
    Cir.addEventListener("click", function(){
        curTool = "circle"; 
        Eraser.classList.remove("selected");
        Brush.classList.remove("selected");
        Reset.classList.remove("selected");
        Save.classList.remove("selected");
        Rect.classList.remove("selected");
        Text.classList.remove("selected");
        this.classList.add("selected");
        remove_canvas(canvas);
        remove_rect(canvas);
        remove_text(canvas);
        remove_tri(canvas);
        add_cir(canvas);
        
    });

    var Tri = document.querySelector("#triangle");
    Tri.addEventListener("click", function(){
        curTool = "triangle"; 
        //alert("tri");
        Eraser.classList.remove("selected");
        Brush.classList.remove("selected");
        Reset.classList.remove("selected");
        Save.classList.remove("selected");
        Rect.classList.remove("selected");
        Text.classList.remove("selected");
        Cir.classList.remove("selected");
        this.classList.add("selected");
        remove_canvas(canvas);
        remove_rect(canvas);
        remove_text(canvas);
        add_tri(canvas);
        remove_cir(canvas);
        
        
    });

    var Rect = document.querySelector("#rectangle");
    Rect.addEventListener("click", function(){
        curTool = "rectangle"; 
        //alert("rect");
        Eraser.classList.remove("selected");
        Brush.classList.remove("selected");
        Reset.classList.remove("selected");
        Save.classList.remove("selected");
        Tri.classList.remove("selected");
        Text.classList.remove("selected");
        Cir.classList.remove("selected");
        this.classList.add("selected");
        remove_canvas(canvas);
        add_rect(canvas);
        remove_text(canvas);
        remove_tri(canvas);
        remove_cir(canvas);
    });

    var Save = document.querySelector("#save");
    Save.addEventListener("click", function(){
        curTool = "save"; 
        //alert("save");
        Eraser.classList.remove("selected");
        Brush.classList.remove("selected");
        Reset.classList.remove("selected");
        Rect.classList.remove("selected");
        Tri.classList.remove("selected");
        Text.classList.remove("selected");
        Cir.classList.remove("selected");
        this.classList.add("selected");
        download_image();
        remove_canvas(canvas);
        remove_rect(canvas);
        remove_text(canvas);
        remove_tri(canvas);
        remove_cir(canvas);
    });
    
    var Reset = document.querySelector("#reset");
    Reset.addEventListener("click", function(){
        curTool = "reset"; 
        //alert("reset");
        history.saveState(canvas);
        Eraser.classList.remove("selected");
        Brush.classList.remove("selected");
        Save.classList.remove("selected");
        Rect.classList.remove("selected");
        Tri.classList.remove("selected");
        Text.classList.remove("selected");
        Cir.classList.remove("selected");
        this.classList.add("selected");
        context.clearRect(0, 0, canvas.width, canvas.height);
        remove_canvas(canvas);
        remove_rect(canvas);
        remove_text(canvas);
        remove_tri(canvas);
        remove_cir(canvas);
    });

    var Brush = document.querySelector("#brush");
    Brush.addEventListener("click", function(){
        curTool = "brush"; 
        //alert("hello");
        Eraser.classList.remove("selected");
        Reset.classList.remove("selected");
        Save.classList.remove("selected");
        Rect.classList.remove("selected");
        Tri.classList.remove("selected");
        Text.classList.remove("selected");
        Cir.classList.remove("selected");
        this.classList.add("selected");
        add_canvas(canvas);
        remove_rect(canvas);
        remove_text(canvas);
        remove_tri(canvas);
        remove_cir(canvas);
    });

    var Eraser = document.querySelector("#eraser");
    Eraser.addEventListener("click", function(){
        curTool = "eraser"; 
        //alert("eraser"); 
        Brush.classList.remove("selected");
        Reset.classList.remove("selected");
        Save.classList.remove("selected");
        Rect.classList.remove("selected");
        Tri.classList.remove("selected");
        Text.classList.remove("selected");
        Cir.classList.remove("selected");
        this.classList.add("selected");
        add_canvas(canvas);
        remove_rect(canvas);
        remove_text(canvas);
        remove_tri(canvas);
        remove_cir(canvas);
    });


    var radius = 5;
    var dragging = false;
    var hasInput = false;
    var rect = {};
    var tri = {};
    var cir = {};
    var restorePoints = [];
    // var start = 0; //0 rad
    // var end = Math.PI * 2; //2pi rad

    var addInput = function(e) {
    
        if(hasInput)
            return;
        
        history.saveState(canvas);
        var input = document.createElement("input");
        var x = e.pageX - this.offsetLeft;
        var y = e.pageY - this.offsetTop;
        
        input.type = "text";
        input.style.position = "fixed";
        input.style.left = (x) + 'px';
        input.style.top = (y) + 'px';
    
        input.onkeydown = handleEnter;
        
        document.body.appendChild(input);
    
        input.focus();
        
        hasInput = true;
    }
    
    function handleEnter(e) {
        var keyCode = e.keyCode;
        if (keyCode === 13) {
            drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
            document.body.removeChild(this);
            hasInput = false;
        }
    }
    
    function drawText(txt, x, y) {
        if(document.getElementById("fontSize").value == "fontSmall")
        {
            var fontSize = "16px ";
        }
        else if(document.getElementById("fontSize").value == "fontMedium")
        {
            var fontSize = "64px ";
        }
        else
        {
            var fontSize = "150px ";
        }

        if(document.getElementById("fontFamily").value == 'serif')
        {
            var fontFamily = "serif";
        }
        else if(document.getElementById("fontFamily").value == 'Yatra One')
        {
            var fontFamily = "'Yatra One', cursive";
        }
        else
        {
            var fontFamily = "'Indie Flower', cursive";
        }
        context.textBaseline = 'top';
        context.textAlign = 'left';
        context.font = fontSize + fontFamily;
        context.fillStyle = document.getElementById("selectColor").value;
        if(document.getElementById("fontSize").value == "fontSmall")
        {
            context.fillText(txt, x-4, y-4);
        }
        else if(document.getElementById("fontSize").value == "fontMedium")
        {
            context.fillText(txt, x, y-30);
        }
        else
        {
            context.fillText(txt, x+5, y-60);
        }
        //context.fillText(txt, x-100, y-100);
    }


    var putPoint = function(event)
    {
        var x = event.pageX - this.offsetLeft;
        var y = event.pageY - this.offsetTop;
        
        if(dragging)
        {
            context.lineTo(x, y);
            if(document.querySelector("#brushSize").value == "brushSmall")
            {
                context.lineWidth = radius;
            }
            else if(document.querySelector("#brushSize").value == "brushMedium")
            {
                context.lineWidth = radius + 5;
            }
            else
            {
                context.lineWidth = radius + 10;
            }
            
            context.lineCap = 'round';
            if(curTool == "eraser")
            {
                context.strokeStyle= "white";
            }
            else
            {
                context.strokeStyle = document.getElementById("selectColor").value;
            }
            context.stroke(); /*actually draws the path you have defined with all those moveTo() and lineTo() methods. The default color is black.*/
            //context.arc(x, y, radius, start, end);
            //context.fill();
            
            context.beginPath();
            context.moveTo(x, y);
        }
        
    }

    var engage = function(e)
    {
        
        dragging = true;
        putPoint(e); // solve the problem of there is no point when only one click
        history.saveState(canvas);
    }

    var disengage = function()
    {
        dragging = false;
        context.beginPath();
    }


    var rectDown = function(e)
    {
        rect.startX = e.pageX - this.offsetLeft;
        rect.startY = e.pageY - this.offsetTop;
        dragging = true;
        history.saveState(canvas);
    }

    var rectUp = function(e)
    {
        dragging = false;
        
    }
    
    var putRect = function(e)
    {
        if (dragging) 
        {
            rect.w = (e.pageX - this.offsetLeft) - rect.startX;
            rect.h = (e.pageY - this.offsetTop) - rect.startY ;
            drawRect();
        }
    }

    function drawRect() 
    {
        context.fillStyle = document.getElementById("selectColor").value;
        context.fillRect(rect.startX, rect.startY, rect.w, rect.h);  
    }

    // var triDown = function(e)
    // {
    //     tri.startX = e.pageX - this.offsetLeft;
    //     tri.startY = e.pageY - this.offsetTop;
    //     dragging = true;
    // }

    // var triUp = function(e)
    // {
    //     dragging = false;
    // }

    var putTri =  function(e)
    {
        tri.startX = e.pageX - this.offsetLeft;
        tri.startY = e.pageY - this.offsetTop;    
        tri.triangleWidth = 100;
        tri.triangleHeight = 100;

        context.beginPath();
        context.moveTo(tri.startX, tri.startY);
        context.lineTo(tri.startX - 100, tri.startY + 100);
        context.lineTo(tri.startX + 100, tri.startY + 100);
        context.closePath();
        context.fillStyle = document.getElementById("selectColor").value;
        context.fill();
        
    }

    var putCir =  function(e)
    {
        cir.startX = e.pageX - this.offsetLeft;
        cir.startY = e.pageY - this.offsetTop;    
        context.beginPath();
        context.arc(cir.startX, cir.startY, 40, 0, 2 * Math.PI);
        context.fillStyle = document.getElementById("selectColor").value;
        context.fill();
    }

    var savePic = function(e)
    {
        history.saveState(canvas);
    }

    var savePicTri = function(e)
    {
        history.saveState(canvas);
    }



    function readImage() {
        if ( this.files && this.files[0] ) {
            var FR= new FileReader();
            FR.onload = function(e) {
               var img = new Image();
               img.addEventListener("load", function() {
                 context.drawImage(img, 0, 0, canvas.width, canvas.height);
               });
               img.src = e.target.result;
            };       
            FR.readAsDataURL( this.files[0] );
        }
    }
    

    function add_img()
    {
        //alert("add_img");
        history.saveState(canvas);
        document.querySelector("#uploadBtn").addEventListener("change", readImage);
    }

    function add_cir(canvas)
    {
        canvas.addEventListener("mousedown", savePic);
        canvas.addEventListener("click", putCir);
    }

    function remove_cir(canvas)
    {
        canvas.removeEventListener("mousedown", savePic);
        canvas.removeEventListener("click",  putCir);
    }

    function add_tri(canvas)
    {
        canvas.addEventListener("mousedown", savePicTri);
        canvas.addEventListener("mouseup", putTri);
        
    }

    function remove_tri(canvas)
    {
        canvas.removeEventListener("mousedown", savePicTri);
        canvas.removeEventListener("mouseup", putTri);
    }

    function add_rect(canvas)
    {
        canvas.addEventListener("mousedown", rectDown);
        canvas.addEventListener("mousemove", putRect);
        canvas.addEventListener("mouseup", rectUp);
    }

    function add_text(canvas)
    {
        canvas.addEventListener("click", addInput);
    }

    function remove_text(canvas)
    {
        canvas.removeEventListener("click", addInput);
    }

    function remove_rect(canvas)
    {
        canvas.removeEventListener("mousedown", rectDown);
        canvas.removeEventListener("mousemove", putRect);
        canvas.removeEventListener("mouseup", rectUp);
    }
   

    function remove_canvas(canvas)
    {
        canvas.removeEventListener("mousedown", engage);
        canvas.removeEventListener("mousemove", putPoint);
        canvas.removeEventListener("mouseup", disengage);
    }

    function add_canvas(canvas)
    {
        
        canvas.addEventListener("mousedown", engage);
        canvas.addEventListener("mousemove", putPoint);
        canvas.addEventListener("mouseup", disengage);
    }

    var history = {
        redo_list: [],
        undo_list: [],
        saveState: function(canvas, list) {
            (list || this.undo_list).push(canvas.toDataURL()); //if parameter list is not passed into this function, data is push into this.undo_list array, otherwise is push into list array
        },
        undo: function(canvas, context) {
            //alert("undo");
          this.restoreState(canvas, context, this.undo_list, this.redo_list);
        },
        redo: function(canvas, context) {
          this.restoreState(canvas, context, this.redo_list, this.undo_list);
        },
        restoreState: function(canvas, context,  pop, push) {
            //alert("undo2");
          if(pop.length) {
            //alert(pop.length);
            this.saveState(canvas, push);
            var restore_state = pop.pop();
            var img = new Image();
            img.src = restore_state;
            context.clearRect(0, 0, canvas.width, canvas.height);
            img.onload = function() {
                context.drawImage(img, 0, 0);  
            }
          }
        }
      }
}

function download_image(){
    var canvas = document.querySelector("#canvas");
    var image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream"); 
    window.location.href = image;
};

